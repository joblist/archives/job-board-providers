> This project is now unmaintained, and deprecated in favor of
> [@joblist/components](https://gitlab.com/joblist/components) with
> which it has been merged. See the documentation for the `job-board`
> web-component.


# job-board-providers (deprecated/unmaintained)

web-components (browser) and node modules to communicate with
job-board providers APIs, and list the jobs of companies.

# Installation

`npm add --save @joblist/job-board-providers`

# Usage in browser

You will need to import the script in your code, with one of these import:

```
<script type="module" src="./src/index.js"></script>
```

## web-components

A few convenience web components are defined.

```
<job-list
	provider-name="greenhouse"
	provider-hostname="gnosis"
></job-list>
```

```
<job-list-greenhouse hostname=""></job-list-greenhouse>
<job-list-personio hostname=""></job-list-personio>
<job-list-recruitee hostname=""></job-list-recruitee>
<job-list-smartrecruiters hostname=""></job-list-smartrecruiters>
```

For the full list of supported providers, check out the `./src/apis`
folder, where all the known and supported providers are listed.

Open an issue on this repository if there is any bug, and to
request/implement new job board providers.

# Usage in node

This repo has no build dependency, is developed browser first, and
does not provide polyfills for certain browser methods.

You can create the utility files and import in your global `/index.js` file (or where you need it).

Global dependency for all providers

- install: `npm install --save node-fetch`
- then `import './utils/fetch-polyfill.js`

```
// ./fetch-polyfill.js
import fetch from 'node-fetch'

/* make fetch available globally */
if (!globalThis.fetch) {
	globalThis.fetch = fetch
}

export default {}
```

Dependency for the `personio` provider:

- install: `npm install --save jsdom`
- then `import './utils/domparser-polyfill.js'`

```
import jsdom  from 'jsdom'
const { JSDOM } = jsdom

/* make DOMParser available globally */
global.DOMParser = new JSDOM().window.DOMParser
```

# Development

To run a local development server:

- `npm install`, run once to install development dependencies
- `npm run develop`, to run the local development server

To see your changes, refresh the browser tab, or restart the development server.

## Build for production

There is not build step, no minification, the project is deployed as is.

## Deployment to production

To deploy a new version to npm, create a new git tag ex `v0.0.1` (needed `vXX.XX.XX` pattern).

It should be the same as the tag on `package.json.version`, and this
will run the `publish` to npm job in `./gitlab-ci.yml`.

The `NPM_TOKEN` is defined at the organisation level.

Note that the variable is available only:
- on protected branch `main`
- on protected tag `v*`

