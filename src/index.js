/* general web-components */
import components from "./components/index.js";
import providerApis from "./apis/index.js";

/* define elements as convenience when importing whole module */
const { componentDefinitions, providerDefinitions } = components;

/* auto define all components, if in browser */
export function defineComponents(components = componentDefinitions) {
	const isBrowser = typeof window !== "undefined";
	if (!isBrowser) return;
	Object.entries(components).map(([cTag, cDef]) => {
		if (!customElements.get(cTag)) {
			customElements.define(cTag, cDef);
		}
	});
}

defineComponents(providerDefinitions);
defineComponents(componentDefinitions);

export default {
	defineComponents,
	componentDefinitions,
	providerDefinitions,
	providerApis,
};
